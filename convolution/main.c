#include <stdio.h>

#define SIGNAL_LENGTH           320
#define IMPULSE_RESPONSE_LENGTH 29

extern double impulse_response[IMPULSE_RESPONSE_LENGTH];
extern double input_signal_1khz_15khz[SIGNAL_LENGTH];

int main() {
    FILE* impulse_response_file = fopen("impulse_response.dat", "w");
    FILE* input_signal_file = fopen("input_signal.dat", "w");

    for (int i = 0; i < IMPULSE_RESPONSE_LENGTH; ++i) {
        fprintf(impulse_response_file, "%f\n", impulse_response[i]);
    }
    fclose(impulse_response_file);

    for (int i = 0; i < SIGNAL_LENGTH; ++i) {
        fprintf(input_signal_file, "%f\n", input_signal_1khz_15khz[i]);
    }
    fclose(input_signal_file);

    return 0;
}
